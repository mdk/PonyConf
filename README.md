# PonyConf

Organise your conferences.


## HowTo Test

(you should work on a virtualenv)

```bash
git clone --recursive git@git.afpy.org:AFPy/PonyConf.git
cd PonyConf
pip install -U -r requirements.txt
./manage.py migrate
./manage.py runserver
./manage.py createsuperuser
```


## HowTo update translations

```bash
./manage.py makemessages
poedit locale/fr/LC_MESSAGES/django.po
./manage.py compilemessages
```
